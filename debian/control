Source: kdepim-addons
Section: kde
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cargo,
               cmake,
               corrosion,
               debhelper-compat (= 13),
               discount,
               doxygen,
               graphviz,
               kaddressbook-dev,
               kf6-extra-cmake-modules,
               kf6-kcmutils-dev,
               kf6-kconfig-dev,
               kf6-kdbusaddons-dev,
               kf6-kdeclarative-dev,
               kf6-kholidays-dev,
               kf6-ki18n-dev,
               kf6-ktextaddons-dev,
               kf6-kxmlgui-dev,
               kf6-prison-dev,
               kpim6-akonadi-dev,
               kpim6-akonadi-notes-dev,
               kpim6-calendarsupport-dev,
               kpim6-eventviews-dev,
               kpim6-grantleetheme-dev,
               kpim6-importwizard-dev,
               kpim6-incidenceeditor-dev,
               kpim6-itinerary-dev,
               kpim6-kontactinterface-dev,
               kpim6-kpimtextedit-dev,
               kpim6-ktnef-dev,
               kpim6-libgravatar-dev,
               kpim6-libkdepim-dev,
               kpim6-libkgapi-dev,
               kpim6-libkleo-dev,
               kpim6-libksieve-dev,
               kpim6-mailcommon-dev,
               kpim6-mailimporter-dev,
               kpim6-messagelib-dev,
               kpim6-mimetreeparser-dev,
               kpim6-pimcommon-dev,
               kpim6-pkpass-dev,
               libgpgme-dev | libgpgme11-dev,
               libmarkdown2-dev,
               libpoppler-qt6-dev,
               libqgpgmeqt6-dev,
               pkg-kde-tools-neon,
               plasma-activities-dev
               qt6-base-dev,
               qt6-webchannel-dev,
               qt6-webengine-dev,
               rustc
Standards-Version: 4.6.2
Homepage: https://pim.kde.org/
Vcs-Browser: https://anonscm.debian.org/git/pkg-kde/applications/kdepim-addons.git
Vcs-Git: https://anonscm.debian.org/git/pkg-kde/applications/kdepim-addons.git

Package: kpim6-kdepim-addons
Architecture: any
Depends: libplasma6,
         qml6-module-qtquick-controls,
         qml6-module-qtquick-layouts,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: kdepim-addons
Description: Addons for KDE PIM applications
 KDE (the K Desktop Environment) is a powerful Open Source graphical
 desktop environment for Unix workstations. It combines ease of use,
 contemporary functionality, and outstanding graphical design with the
 technological superiority of the Unix operating system.
 .
 This package includes a collection of extensions for the Personal Information
 Management (PIM) desktop applications provided with the official release of
 KDE Applications.

Package: kdepim-addons
Architecture: all
Depends: kpim6-kdepim-addons, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.